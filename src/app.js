import { Article } from './article'
import { authWithEmailAndPassword, getAuthForm } from './auth'
import { createModal, isValid } from './utils'
import './style.css'

const form = document.getElementById('form')
const modalBtn = document.getElementById('modal-btn')
const input = form.querySelector('#article-add')
const submitBtn = form.querySelector('#submit')

window.addEventListener('load', Article.renderList)
modalBtn.addEventListener('click', openModal)
form.addEventListener('submit', submitFormHandler)
input.addEventListener('input', () => {
    submitBtn.disabled = !isValid(input.value)
})

function submitFormHandler(event) {
    event.preventDefault()

    if (isValid(input.value)) {
        const article = {
            text: input.value.trim(),
            date: new Date().toJSON()
        }


        submitBtn.disabled = true
        //Async request to server
        Article.create(article).then(() => {
            input.value = ""
            input.className = ""
            submitBtn.disabled = false
        })
    }
}

function openModal() {
    createModal('sign in', getAuthForm())
    document.getElementById('auth-form').addEventListener('submit', authFormHandler, { once: true })
}

function authFormHandler(event) {
    event.preventDefault()

    const btn = event.target.querySelector('button')
    const email = event.target.querySelector('#email').value
    const password = event.target.querySelector('#password').value

    btn.disabled = true
    authWithEmailAndPassword(email, password)
        .then(Article.fetch)
        .then(renderModalAfterAuth)
        .then(() => btn.disabled = false)
}

function renderModalAfterAuth(content) {
    if (typeof content === 'string') {
        createModal('Error', content)
    } else {
        createModal('Articles List', Article.listToHTML(content))
    }
}