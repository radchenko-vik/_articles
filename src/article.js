export class Article {
    static create(article) {
        return fetch('https://articles-app-2aee2-default-rtdb.firebaseio.com/articles.json', {
            method: 'POST',
            body: JSON.stringify(article),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(response => {
                article.id = response.name
                return article
            })
            .then(addToLocalStorage)
            .then(Article.renderList)
    }
    static renderList() {
        const articles = getArticlesFromLocalStorage()

        const html = articles.length
            ? articles.map(toCard).join('')
            : `<div class="mui--text-headline">Empty</div>`
        const list = document.getElementById('list')
        list.innerHTML = html
    }
    static fetch(token) {
        if (!token) {
            return Promise.resolve('<p class="error">missing token</p>')
        }
        return fetch(`https://articles-app-2aee2-default-rtdb.firebaseio.com/articles.json?auth=${token}`)
            .then(response => response.json())
            .then(response => {
                if (response && response.error) {
                    return `<p class="error">${response.error}</p>`
                }

                return response ? Object.keys(response).map(key => ({
                    ...response[key],
                    id: key
                })) : []
            })
    }

    static listToHTML(articles) {
        return articles.length
            ? `<ol>${articles.map(a => `<li>${a.text}</li>`).join('')}</ol>`
            : '<p>no articles</p>'
    }

}
function addToLocalStorage(article) {
    const all = getArticlesFromLocalStorage()
    all.push(article)
    localStorage.setItem('articles', JSON.stringify(all))
}
function getArticlesFromLocalStorage() {
    return JSON.parse(localStorage.getItem('articles') || '[]')
}

function toCard(article) {
    return `
    <div class="mui--text-black-54">
    ${new Date(article.date).toLocaleDateString()}
    ${new Date(article.date).toLocaleTimeString()}
    </div>
    <div>${article.text}</div>
    <br>
          `
}